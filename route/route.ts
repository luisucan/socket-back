import { Router,Request,Response} from 'express';
import Server from '../class/Server';
import { Socket } from 'socket.io';
import { usuarioConectados } from '../sockets/socket';

let router = Router();

router.post('/mensajes',( req:Request,res:Response )=>{
    const cuerpo = req.body.cuerpo;
    const de = req.body.de;

    let payload = {
        de,cuerpo
    };

    const server = Server.instance;
    server.io.emit('mensaje-nuevo',payload );

    res.json({
        ok:true,
        cuerpo,de
    })
});

router.post('/mensajes/:id',( req:Request,res:Response )=>{
    const cuerpo = req.body.cuerpo;
    const de = req.body.de;
    const id = req.params.id;

    let payload = {
        de,cuerpo
    };

    const server = Server.instance;
    server.io.in( id ).emit('mensaje-privado',payload );

    res.json({
        ok:true,
        cuerpo,de,id
    })
});

router.get('/usuarios',( req:Request,res:Response )=>{
    

    const server = Server.instance;
    server.io.clients( ( err:any, clientes:string[])=>{
        if( err ){
            return res.json({
                ok:false,
                err
            })        
        }
        res.json({
            ok:true,
            clientes
        })
    });

});

router.get('/usuarios/detalle',( req:Request,res:Response )=>{
    

    res.json({
        ok:true,
        clientes: usuarioConectados.getLista()
    })

});

export default router;