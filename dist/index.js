"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Server_1 = __importDefault(require("./class/Server"));
const cors_1 = __importDefault(require("cors"));
const route_1 = __importDefault(require("./route/route"));
const serve = Server_1.default.instance;
serve.app.use(cors_1.default({ origin: true, credentials: true }));
serve.app.use(route_1.default);
serve.start(() => {
    console.log('servidor corriendo en el puerto ' + serve.port);
});
