"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const usuarios_lista_1 = require("../class/usuarios-lista");
const usuario_1 = require("../class/usuario");
exports.usuarioConectados = new usuarios_lista_1.UsuariosLista();
exports.conectarCliente = (cliente, io) => {
    const usuario = new usuario_1.Usuario(cliente.id);
    exports.usuarioConectados.agregar(usuario);
};
exports.desconectar = (cliente, io) => {
    cliente.on('disconnect', () => {
        console.log('cliente desconectado');
        exports.usuarioConectados.borrarUsuario(cliente.id);
        io.emit('usuarios-activos', exports.usuarioConectados.getLista());
    });
};
exports.mensaje = (cliente, io) => {
    cliente.on('mensaje', (payload) => {
        io.emit('mensaje-nuevo', payload);
    });
};
exports.configurarUsuario = (cliente, io) => {
    cliente.on('configurar-usuario', (payload, callback) => {
        console.log('configurando usuario ' + payload.nombre);
        exports.usuarioConectados.actualizarNombre(cliente.id, payload.nombre);
        io.emit('usuarios-activos', exports.usuarioConectados.getLista());
        callback({
            ok: true,
            mensaje: `Usuario ${payload.nombre} configurado con exito `
        });
    });
};
exports.obtenerUsuario = (cliente, io) => {
    cliente.on('obtener-usuarios', () => {
        io.to(cliente.id).emit('usuarios-activos', exports.usuarioConectados.getLista());
    });
};
