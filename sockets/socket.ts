import { Socket } from "socket.io";
import { UsuariosLista } from "../class/usuarios-lista";
import { Usuario } from "../class/usuario";

export const usuarioConectados = new UsuariosLista();

export const conectarCliente = ( cliente:Socket,io:SocketIO.Server )=>{
    const usuario = new Usuario( cliente.id );
    usuarioConectados.agregar( usuario );
    
};

export const desconectar = ( cliente:Socket,io:SocketIO.Server ) =>{
    cliente.on('disconnect',()=>{
        console.log('cliente desconectado');
        usuarioConectados.borrarUsuario( cliente.id );
        io.emit('usuarios-activos',usuarioConectados.getLista());
    });
};

export const mensaje = ( cliente:Socket,io:SocketIO.Server )=>{
    cliente.on('mensaje',( payload:{de:string,cuerpo:string})=>{
        
        io.emit('mensaje-nuevo', payload);
    });
};

export const configurarUsuario = ( cliente:Socket,io:SocketIO.Server )=>{
    cliente.on('configurar-usuario',( payload:{ nombre:string },callback:Function )=>{
        console.log('configurando usuario '+payload.nombre );
        usuarioConectados.actualizarNombre( cliente.id,payload.nombre );
        io.emit('usuarios-activos',usuarioConectados.getLista());
        callback({
            ok:true,
            mensaje:`Usuario ${payload.nombre} configurado con exito `
        })
    });
};

export const obtenerUsuario = ( cliente:Socket,io:SocketIO.Server )=>{
    cliente.on('obtener-usuarios',(  )=>{
        io.to( cliente.id ).emit('usuarios-activos',usuarioConectados.getLista());
    });
};