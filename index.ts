import Server from "./class/Server";
import cors from 'cors';
import router from "./route/route";

const serve = Server.instance;

serve.app.use( cors({ origin:true,credentials:true }) );

serve.app.use( router );

serve.start( ()=>{
    console.log('servidor corriendo en el puerto '+serve.port);
});